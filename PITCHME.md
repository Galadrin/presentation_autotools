---?image=images/meetup_front_cover.jpeg
<!-- $theme: gaia -->
<!-- *template: invert -->
<!-- $width: 12in -->
## Présentation des outils de build logiciel

<!-- footer:  David Pierret 20/06/2018 -->
<img style="position:fixed; right: 40px;bottom: 21px"  src="images/smile_logo_full.png">

---

## David Pierret

* Ingénieur développement embarqué chez Smile depuis 2 ans
  * Développement Kernel Linux et Driver
  * Développement BSP (Buildroot, Yocto, custom)
* 10 ans d'experience
  * Intel NDG : Développement framework de tests
  * Sanimat-Santé : Applicatif Android
  * Navocap : Maintenance BSP (OpenEmbedded)

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

## Sommaire

* Qu'est qu'un outil de build  logiciel
* Pourquoi les utiliser
* Autotools
* CMake
* Meson
* apperçu de Ninja

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

# ==Qu'est qu'un outil de build==
## Pourquoi les utiliser
![bg](images/adventure-alps-climb-314703.jpg)

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

>==définition :==
>Un outil de build logiciel est un programme ou un ensemble de programmes, facilitant la compilation des sources d'un projet.

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

* ==Respect des standards de projet GNU ou assimilés.==<br>Facilite sa diffusion.
* ==Apporte une plus grande portabilité du logiciel==
* ==Aide à la configuration==
  * quel est le compilateur
  * quel est le system cible et  les fonction disponibles
* ==Make==
  * Ou sont les librairies et dépendances
  * Ou installer le programme

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

* ==pourquoi ne pas écrire ses propres Makefiles==
  * Respect des variables et ***targets*** standard
  * Position des headers et libraries
  * Maintenance difficile
  * Portabilité médiocre

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
 
* ==portabilité==
  * Les outils fonctionnent sur différents OS
  * Se base sur l'experience de la communauté
* ==Vérification des dépendances avant compilation==
  * capable de vérifier la présence des librairies et leurs capacité

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
<!-- *template: invert -->
# <center>Les autotools</center>
# <center>![80%](images/Heckert_GNU_white.svg.png)</center>

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
#### 3 programmes principaux composent les autotools
* ==**Autconf :**== Génère le script de configuration
* ==**Automake :**== Écrit les Makefile’s
* ==**Libtool :**== Applique un ensemble de modifications pour la création de librairies
---
#### Historique
* ==**Origine :**==  Un script ***configure*** transforme un `Makefile.in` en ***Makefile***
* ==**Autoconf 1 (1992) :**==  **Autoconf** transforme `Configure.in` en ***configure***
* ==**Autoconf 2 (1994) :**==  Ajout de la cross-compilation, de la gestion des sous répertoires, du cache…
* ==**Automake (1994) :**==  **Automake** transforme `Makefile.in` en ***Makefile***
* ==**Libtool (1996) :**==  Change le comportement d’**Automake** pour la compilation de librairie

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
<div style="position:fixed; top:0; left:33%">
    <img width="65%" height="50%" src="images/Autoconf-automake-process.svg.png">
</div>
<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Autoconf

* ==configure.ac==
  * Écrit par l’utilisateur
  * interprété par GNU M4
  * Défini les action à effectuer à l’aide de macro de contrôle, de tests et d’actions.
  * Est utilisé pour créer ***configure***

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### Autoconf
* ==configure==
  * Script shell "standard"
  * Test et génère ***config.status***

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Automake
* ==Makefile.am==
  * Fichier écrit par l’utilisateur.
  * Template pour le Makefile final
  * Possède la même syntaxe qu’un Makefile ordinaire
  * Défini des variable spéciales, interprété par Automake

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### Automake
* ==Makefile.in==
  * Fichier interpreté par ***configure*** pour créer le ***Makefile***

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

# Exemple

* Configure.ac
```configure.ac
AC_INIT([amhello], [1.0], [bug-automake@gnu.org])
AM_INIT_AUTOMAKE([-Wall -Werror foreign])
AC_PROG_CC
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([
 Makefile
 src/Makefile
])
AC_OUTPUT
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### Exemple (suite)
* Makefile.am
```
SUBDIRS = src
```
* src/Makefile.am
```
bin_PROGRAMS = hello
hello_SOURCES = main.c
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

# Exemple (suite)
* <small>Autoreconf</small>
```bash
configure.ac: installing './install-sh'
configure.ac: installing './missing'
configure.ac: installing './compile'
src/Makefile.am: installing './depcomp'
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
<small>* Configure
```bash
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for gawk... no
checking for mawk... mawk
checking whether make sets $(MAKE)... yes
checking for gcc... gcc
checking for C compiler default output file name... a.out
checking whether the C compiler works... yes
checking whether we are cross compiling... no
checking for suffix of executables...
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking for style of include used by make... GNU
checking dependency style of gcc... gcc3
configure: creating ./config.status
config.status: creating Makefile
config.status: creating src/Makefile
config.status: creating config.h
config.status: executing depfiles commands
```
</small>

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

## avantages

* Excellent support pour les plates-formes Unix existantes.
* Grand choix de modules existants.

## inconvenients

* Difficile a correctment utiliser, 
* Compliqué a déboguer
* Support médiocre pour les plates-formes non-Unix (surtout Windows).
* Incompréhensible pour la plupart des gens

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
<!-- *template: invert -->
# CMake
# ![](images/cmake_logo.png)
---

> ==d'après le site officiel :==
>  CMake est une famille d'outils ***open-source et multiplateforme*** conçue pour construire, tester et empaqueter des logiciels. 
>
> CMake est utilisé pour contrôler le processus de compilation du logiciel à l'aide de simples fichiers de configuration ***indépendants de la plate-forme et du compilateur***.

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

#### Historique

* Développé entre 1999 et 2000.
* Répond à un besoin d'environnement de build multi-plateforme.
* A l'origine créé pour des logiciels de graphismes.
* Devenu populaire avec l'adoption par le projet KDE.

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

![300%](images/CMake_process.png)

<small><small>*source: developpez.com*</small></small>

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### CMakeLists.txt

* Se situe à la racine et chaques sous-projets
* Décrit le projet
* Décrit la facon de le compiler
* Possède une syntaxe propre à CMake

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### Informations minimum

* Version minimale de CMake pour lire le fichier CMake <br><small>==cmake_minimum_required()==</small>
* Le nom du projet <br><small>==project()==</small>
* Le binaire à produire <br><small>==add_executable()==/==add_library()==</small>
* Les dépendances du projet <br><small>==target_include_directories()==/==target_link_libraries()==</small>

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### configuration du binaire

* définition d'un templates

|<small>version.h.in</small>|
|  :--        |
|`#define VERSION_MAJOR "@HELLO_VERSION_MAJOR@"`|

* transformation en header
```cmake
project (hello VERSION 1.0.1)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/src/version.h.in \
               ${CMAKE_CURRENT_SOURCE_DIR}/version.h)
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Exemple

* projet en C
* une librairie dans l'arborescence (libhello)
* un programme principal

```text
.
├── CMakeLists.txt
├── libhello
│   ├── CMakeLists.txt
│   ├── hello.c
│   └── hello.h
└── main.c
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Exemple (suite)

* ==CMakeLists.txt== de la librairie
```cmake
cmake_minimum_required(VERSION 3.0)

project(libhello)

set(SRCS
    hello.c
    )
    
set(HEADERS
    hello.h
    )

add_library(hello ${SRCS} ${HEADERS})
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Exemple (suite)

* ==CMakeLists.txt== du programme principal

```cmake
cmake_minimum_required(VERSION 3.0)

project(hello)

add_subdirectory(libhello)

set(SRCS
    main.c
    )
    
add_executable(main ${SRCS})

target_link_libraries(main hello)
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

## Avantages

* décrire un projet basique est assez simple.
* documentation des commandes complète.
* forte portabilité en dehors du monde Unix.
* Supporté par les IDE <small>(Eclipse, Visual Studio, XCode...)</small>.

## inconvenients

* Le langage de script est lourd à utiliser.
* plusieurs facons d'arriver à la meme chose.

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
<!-- *template: invert -->
# <center>Meson</center>
# <center>![200%](images/meson_logo.png)</center>

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

> Meson est un système de construction open source conçu pour être à la fois extrêmement rapide et, plus important encore, aussi convivial que possible.
<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Présentation

* Projet débuté en 2013 par **Jussi Pakkanen**
* Aurait dut s'appeler Gluons, mais le nom etait déja utilisé.
><small><small>Un **méson** est, une particule composite composé de quarks et d'antiquarks</small></small>
* Ecrit en Python
* Axé sur la rapidité et la facilité de mise en oeuvre
* Utilise **Ninja** au lieu de **Make** par défaut
* Déjà adopté par des projets comme systemd et Gnome

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### meson.build

* Fichier de description du projet semblable a CMakeLists.txt
* Utilise la syntaxe de Python
* Fortement typé mais pas de type visible
* Utilise le traitement des variable de Python (strings, dictionnaires, booleens..)

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Informations minimum

* Le nom du projet + language<br><small>==project()==</small>
* Le binaire à produire + fichiers source <br><small>==executable()==/==shared_library()==/==static_library()==</small>
* Les dépendances du projet <br><small>==dependency()==/==find_library()==</small>

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### configuration du binaire

* définition d'un templates

|<small>version.h.in</small>|
|  :--        |
|`#define VERSION_MAJOR "@HELLO_VERSION_MAJOR@"`|

* transformation en header
```meson
conf_data = configuration_data()
conf_data.set('HELLO_VERSION_MAJOR', '1.2.3')
configure_file(input : 'version.h.in',
               output : 'version.h',
               configuration : conf_data)
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Exemple

* projet en C
* une librairie dans l'arborescence (libhello)
* un programme principal

```text
.
├── meson.build
├── libhello
│   ├── meson.build
│   ├── hello.c
│   └── hello.h
└── main.c
```
<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### Exemple (suite)

* ==meson.build== du programme principal
```meson
project('hello', 'c')
src = ['main.c']
inc = include_directories('libhello')

subdir('libhello')

hello_exe = executable('hello', 
                 src, 
                 include_directories : inc
                 link_with : libhello)

test('hello test', hello_exe)
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
### Exemple (suite)

* ==meson.build== de la librairie
```meson
libhello_src = ['hello.c']
libhello = library('libhello', 
                   libhello_src
                   include_directories : inc)
```

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

### Avantages

* plus rapide que les autres systèmes.
* capable de télécharger des dépendances ([Wrap](http://mesonbuild.com/Wrap-dependency-system-manual.html)).
* supporte nativement les outils comme Valgrind, couverture de code etc..

## inconvenients

* Plutôt récent, communauté réduite.

<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---
<!-- *template: invert -->
# Ninja
# ![100%](images/ninja_tux.png)
---

* Créé par les développeurs de Google Chrome qui trouvait la compilation trop lente.
    :arrow_right: **Make** mettait 10 secondes à démarrer

* La première version codé en 1 weekend. 
    :arrow_right: La compilation démarrait en moins d'1 seconde
* N'est pas conçu pour être écrit


<img style="position:fixed; right: 30px;bottom: 21px" width="60px" height="60px" src="images/smile_logo.png">

---

<!-- *template: invert -->

# Questions
